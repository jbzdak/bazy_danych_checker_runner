
Instalacja
==========

Windows
-------

Instalujemy pythona 3.3.0 ze strony: http://www.python.org/download/releases/3.3.0/.

Powinno działać

Linuks
------

**Nowe dystrybucje**

Proszę zainstalować pythona 3.3.0 z paczek (na 3.2.0 też powinno działać)

**Stare dystrybucje**

Można zainstalować pythonbrewm, które 'magicznie' kompiluje pythona ze źródeł:
https://github.com/utahta/pythonbrew.

Używanie
========

Należy wykonać polecenie::

   run_tests.py --settings bd_checker_settings_home [dalej jak zwykle]

na przykład::

   run_tests.py --settings bd_checker_settings_home --user_id 0000 --unit_no 1 --task_no 3 check

