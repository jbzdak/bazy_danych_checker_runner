#!/usr/bin/env python3

# import bd_checker_settings

import sys
import os
import argparse
import shelve
import hashlib
import datetime
import importlib


from argparse import ArgumentParser

import getpass

VERSION = 1

class DataPackage(object):
    def __init__(self, user_id_1, user_id_2, user_pass_1, user_pass_2, unit_no,
                 task_no, super_password, args, kwargs):
        super().__init__()
        self.user_id_1 = user_id_1
        self.user_id_2 = user_id_2
        self.user_pass_1 = user_pass_1
        self.user_pass_2 = user_pass_2
        self.unit_no = unit_no
        self.task_no = task_no
        self.super_password = super_password
        self.args = args
        self.kwargs = kwargs


class SplitArgument(argparse._AppendAction):

    def __call__(self, parser, namespace, values, option_string=None):
        super(SplitArgument, self).__call__(
            parser,
            namespace,
            values.split('='),
            option_string
        )


def is_check_needed(user_id, unit_no, task_no):
    sha = hashlib.sha256()
    sha.update("{}.{}.{}".format(user_id, unit_no, task_no).encode("utf8"))
    return int(sha.hexdigest(), 16) % 10 == 0

parser = ArgumentParser()
subparsers = parser.add_subparsers()

parser.add_argument("--user_id", required="USER_ID_1" not in os.environ,
                          default= os.environ.get("USER_ID_1", None),
                          help="Nazwa pierwszego użytkownika. Można ominąć po ustawieniu zmiennej środowiskowej USER_ID_1")

parser.add_argument("--force_super_check", required=False,
                    default=False,
                    help="Wymusza sprawdzenie przez prowadzącego")

parser.add_argument("--user_id_2", required=False,
                    default=os.environ.get("USER_ID_2", None),
                    help="Nazwa pierwszego użytkownika. Można ominąć po ustawieniu zmiennej środowiskowej USER_ID_2")

parser.add_argument("--unit_no", required="UNIT_NO" not in os.environ, type=int,
                    default=int(os.environ.get("UNIT_NO", "1")),
                    help="Numer zajęć, rozpoczynając od 1. Możesz ominąć po ustawieniu zmiennej UNIT_NO")

parser.add_argument("--task_no", required=True, type=int,
                          help="Numer zadania, rozpoczynając od 1")

parser.add_argument("--settings", default="bd_checker_settings",
                    help="Settings file to use")

check_parser = subparsers.add_parser("check", help="Wykonuje zadanie")
check_parser.set_defaults(action = "check")

check_parser.add_argument("--file", action=SplitArgument,
                         help="Podaj plik do sprawdzenia, składnia "
                                "tego polecenia jest taka że podajesz "
                                "nazwa=ścieżka. Na przykład "
                                "--file pm_10=/tmp/pm10.csv")
check_parser.add_argument("--arg", action=SplitArgument,
                         help="Podaje coś co nie jest plikiem do "
                               "sprawdzenia. składnia "
                               "tego polecenia jest taka że podajesz "
                               "nazwa=ścieżka. Na przykład "
                               "--arg foo=5.1")
check_parser.add_argument("--query", action="store_true", default=False)

query = subparsers.add_parser("query", help = "Kasuje bazę danych zapamiętanych haseł")

query.set_defaults(action="query")

set_password = subparsers.add_parser("set_password")
set_password.set_defaults(action="set_password")


show_marks = subparsers.add_parser("show_marks")
show_marks.set_defaults(action="show_marks")
show_marks.add_argument("--details", action="store_true", default=False)


def _read_password_from_user(user_id, unit_id=None):
    password = getpass.getpass("Podaj hasło dla użytkownika {}: ".format(
        user_id))
    hasher = hashlib.sha256()
    key = "{}.{:%Y-%m-%d}.{}".format(
        unit_id, datetime.date.today(), password).encode("utf-8")
    hasher.update(key)

    if unit_id:
        data = {}
        return {
            "digest": hasher.hexdigest(),
            "date": datetime.date.today()
        }

    return password


def _read_password_from_db(user_id, unit_id):
    return None


def get_password(user_id, unit_id=None):
    if unit_id is not None and _read_password_from_db(user_id, unit_id):
        return _read_password_from_db(user_id, unit_id)
    return _read_password_from_user(user_id, unit_id)


def create_pyro_proxy(config):
    import Pyro4

    Pyro4.config.HMAC_KEY = config.HMAC_KEY
    uri = "PYRO:executor@{}:{}".format(config.HOST,
                                       config.PORT)

    # print(uri)
    return Pyro4.Proxy(uri)


def get_query(arg_dict):
    query = []
    line = ""

    print("Podaj zapytanie (może mieć wiele linii, zakończ pustą linią)")
    line = sys.stdin.readline()
    while len(line) > 1:
        # print(len(line))
        query.append(line)
        line = sys.stdin.readline()
    arg_dict['query'] = "".join(query)


def do_magic():

    args = parser.parse_args()

    settings = importlib.import_module(args.settings)

    proxy = create_pyro_proxy(settings)

    if settings.USE_PYRO and proxy.version() != VERSION:
        print("\n\nUwaga! Używasz starej wersji oprogramowania, pobierz nową z "
              "https://bitbucket.org/jbzdak/bazy_danych_checker")

    arg_dict = {}
    if args.action == "show_marks":
        password = get_password(args.user_id)
        if args.details:
            print("Pierwsza kolumna: zajęcia, druga: zadanie, ocena")
        else:
            print("Pierwsza kolumna: zajęcia, druga suma ocen za zasdania. "
                  "Proszę samodzielnie podzielić przez ilość zadań "
                  "do wykonania")

        results = proxy.show_marks(args.user_id, password, args.details)
        for row in results:
            print(row)
        return

    super_password = None

    if args.action == 'check':

        if args.file:
            for name, file_name in args.file:
                with open(file_name) as f:
                    arg_dict[name] = f.read()

        if args.arg:
            arg_dict.update(dict(args.arg))

        if args.query:
            get_query(arg_dict)

    elif args.action == 'query':
        print("Polecenie query będzie usunięte proszę używać 'check --query'")
        get_query(arg_dict)

    pack = DataPackage(
        args.user_id,
        args.user_id_2,
        None,
        None,
        args.unit_no,
        args.task_no,
        super_password,
        [],
        arg_dict
    )

    if not settings.USE_PYRO:
        from bdchecker.unit_util import execute_test_checker
        passes, mark, result = execute_test_checker(pack)
    else:

        passes, mark, result = proxy.execute_test_checker(pack.__dict__, True)


    if result:
        print(result)

    if passes:
        print("Zadanie zaliczone z oceną {}".format(mark/2.0))
    else:
        print("Zadanie nie zaliczone")

    print("Oceniono {} i {}".format(args.user_id, args.user_id_2))


if __name__ == "__main__":
    do_magic()